package com.example.sv.webpdf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private WebView mWvContent;
    private ValueCallback<Uri[]> mFileCallback;
    private EditText mEdtAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mWvContent = (WebView) findViewById(R.id.wv_content);
        mEdtAddress = (EditText) findViewById(R.id.et_address);
        mEdtAddress.setText("https://jobs.lever.co/lever/f366f610-1af4-43c1-9213-9c39830a715f/apply");
        ImageView mIvGo = (ImageView) findViewById(R.id.iv_go);
        mIvGo.setOnClickListener(this);

        mWvContent.getSettings().setJavaScriptEnabled(true);

        mWvContent.loadUrl("http://fex.net/");

        mWvContent.requestFocus();
        mWvContent.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                mFileCallback = filePathCallback;
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                startActivityForResult(Intent.createChooser(intent, "File Chooser"), 101);
                return true;
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            if (mFileCallback != null) {
                Uri result = data.getData();
                if (result != null) {
                    mFileCallback.onReceiveValue(new Uri[]{result});
                }

            }
        }
    }

    private void showFileDialog() {
        new AlertDialog.Builder(this)
                .setTitle("File chooser")
                .setMessage("Please enter correct WEB address!")
                .setPositiveButton(getResources().getString(android.R.string.ok), null)
                .show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.iv_go) {

            mEdtAddress.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mEdtAddress.getWindowToken(), 0);

            String address = mEdtAddress.getText().toString();
            if (!TextUtils.isEmpty(address) || address.contains("http")) {
                mWvContent.loadUrl(address);
            } else {
                mEdtAddress.setText("");
                showFileDialog();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mWvContent.canGoBack()) {
            mWvContent.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
}
